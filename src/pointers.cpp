#include <iostream>

using namespace std;

void myswap(int* &ptr1, int* &ptr2)
{
  int* temp = ptr1;
  ptr1 = ptr2;
  ptr2 = temp;
}

/*void swap(int &x, int &y){
	int temp = x;
	 x = y;
	 y = temp;
	
	}*/
	
int main()
{
  int a = 25, b = 11;  
  
	int* ptr1 = &a;
	int* ptr2 = &b;  

  cout<<"What am I doing wrong ☹ \n\n";

  cout<<"a = "<<a<<", b = "<<b<<endl;

  //swap(a,b); //why does theirs work????   
  
 
  myswap(ptr1, ptr2); // but mine doesn't ?!?!?!?!!!???

  cout<<"a = "<< *ptr1<<", b = "<< *ptr2<<endl;


}
